package com.ust.testngnew;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

public class AppTest 
{
    App appTest=null;
    @BeforeSuite
    public void setup() {
    	appTest=new App();
    }
    
    @BeforeGroups("calc")
    public void beforeGroupSetup() {
    	System.out.println("Executing group name calc");
    }
    @Test(description = "perform addition of two numbers",groups = "calc",priority = 1)
    
    public void testAdd()
    {
        assertEquals(30, appTest.addition(10, 20));
    }
    @Test(description = "perform addition of two numbers",groups = "calc",priority = 2)
    public void testSub()
    {
        App appTest=new App();
        assertEquals(-10, appTest.subtraction(10,20));
    }
    @Test(description = "check the message")
    public void testMsg()
    {        
        assertEquals("TestNG is a testing framework inspired from JUnit ",new App().message());
    }
    
    @DataProvider(name="calData")
    public Object[][] dataForAddition(){
    	return new Object[][] {
    		{10,20,30},
    		{30,70,100},
    		{30,-60,-30}
    	};
    }
    
    @Test(dataProvider = "calData",description="adition with data provide")
    public void testAdditionDataProvider(int x,int y,int result){           
        assertEquals(result,appTest.addition(x, y));
    } 
    
    @Test
    public void sslc() {
    	System.out.println("Completed sslc");
    }
    
    @Test(dependsOnMethods = "sslc")
    public void hse() {
    	System.out.println("Completed +2");
    }
    
    @Test(dependsOnMethods = "hse")
    public void btech() {
    	System.out.println("Completed Btech");
    }
    
    
}
