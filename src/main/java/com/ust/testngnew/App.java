package com.ust.testngnew;

/**
 * Hello world!
 *
 */
public class App 
{
	public int addition(int x,int y) {
    	return x+y;
    }
    
    public String message() {
    	return "TestNG is a testing framework inspired from JUnit ";
    }

	public Object subtraction(int x, int y) {
		return x-y;
	}
}
